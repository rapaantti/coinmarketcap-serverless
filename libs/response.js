function getHeaders() {
  return {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': true,
  }
}

function getErrorResponse(message) {
  return {
    statusCode: 500,
    headers: getHeaders(),
    body: JSON.stringify({errorMessage: message})
  }
}

module.exports = {
  getHeaders: getHeaders,
  getErrorResponse: getErrorResponse
};
const https = require('https');

function get(url, callback) {
  https.get(url, (response) => {
    let data = '';

    response.on('data', (chunk) => {
      data += chunk;
    });

    response.on('end', () => {
      callback(null, JSON.parse(data));
    })
  });
}

module.exports = {
  get: get
};
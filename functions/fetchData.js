const uuid = require('uuid');
const AWS = require('aws-sdk');
const request = require('../libs/request.js');
const response = require('../libs/response.js');

AWS.config.update({region: process.env.SERVERLESS_REGION});
const dynamoDb = new AWS.DynamoDB.DocumentClient();

function handler(event, context, callback) {
  request.get(process.env.COINMARKETCAP_URL, (error, coinsData) => {

    if (error) {
      callback(null, response.getErrorResponse(error));
    }

    handleSave(coinsData, (error, data) => {
      if (error) {
        callback(null, response.getErrorResponse(error));
      }

      callback(null, {
        statusCode: 200,
        headers: response.getHeaders(),
        body: JSON.stringify(data)
      });
    });
  });
}

function handleSave(coins, callback) {
  coins
    .map((coin) => getSaveableObject(coin))
    .forEach((params) => saveToDynamoDb(params, callback));

  callback(null, coins);
}

function saveToDynamoDb(params, callback) {
  dynamoDb.put(params, (error, data) => {
    if (error) {
      callback(error, null);
    }
  })
}

function getSaveableObject(coin) {
  return {
    TableName: `${process.env.DYNAMODB_TABLE_NAME}-${process.env.SERVERLESS_STAGE}`,
    Item: {
      id: uuid.v1(),
      coinId: coin.id,
      name: coin.name,
      symbol: coin.symbol,
      price_usd: parseFloat(coin.price_usd),
      price_btc: parseFloat(coin.price_btc),
      supply: parseFloat(coin.available_supply),
      updated: parseFloat(coin.last_updated) * 1000,
      inserted: new Date().getTime()
    }
  }
}

module.exports.handler = handler;
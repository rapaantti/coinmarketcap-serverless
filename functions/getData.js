const AWS = require('aws-sdk');
const response = require('../libs/response.js');

AWS.config.update({region: process.env.SERVERLESS_REGION});
const dynamoDb = new AWS.DynamoDB.DocumentClient();

function handler(event, context, callback) {
  const params = {
    TableName: `${process.env.DYNAMODB_TABLE_NAME}-${process.env.SERVERLESS_STAGE}`,
    FilterExpression: "coinId = :coinId",
    ExpressionAttributeValues: {
      ":coinId": event.pathParameters.id
    }
  };

  dynamoDb.scan(params, (error, data) => {
    callback(null, {
      statusCode: 200,
      headers: response.getHeaders(),
      body: JSON.stringify(data.Items)
    });
  });
}

module.exports.handler = handler;
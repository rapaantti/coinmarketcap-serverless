# Coinmarketcap 
## Setup
* [Install AWS CLI](http://docs.aws.amazon.com/cli/latest/userguide/installing.html)
* Create AWS profile: aws configure --profile xxxx (region: eu-central-1, output: 'json') ([See more info](http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html#cli-config-files))
* Install Serverless: `npm i -g serverless`

## Deployment
* Run `serverless deploy --stage xxx`, stage for development / production (fe. dev, prod, etc..)